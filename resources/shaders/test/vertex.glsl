#version 150

in vec2 vertices;
in vec2 textures;

out vec2 textureCoords;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

void main(void) {

    textureCoords = textures;

    gl_Position = view * model * projection * vec4(vertices, 0,1);
}
