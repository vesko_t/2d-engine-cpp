#version 150

uniform sampler2D sampler;

in vec2 textureCoords;

void main(void) {
    gl_FragColor = texture2D(sampler, textureCoords);
}
