//
// Created by vesko on 7.11.2018 г..
//

#include <iostream>
#include <vector>
#include <fstream>
#include "Render.h"
#include <GL/glew.h>
#include <GLM/vec3.hpp>
#include <GLM/gtc/type_ptr.hpp>

Shader::Shader(std::string shaderName) : programId{0}, vertexId{0}, fragmentId{0} {
    this->programId = glCreateProgram();

    this->vertexId = glCreateShader(GL_VERTEX_SHADER);

    std::string vertexString = getShaderSource("resources/shaders/" + shaderName + "/vertex.glsl");
    const char *vertexCode = vertexString.c_str();
    glShaderSource(vertexId, 1, &vertexCode, 0);
    glCompileShader(vertexId);
    GLint isCompiled = 0;
    glGetShaderiv(vertexId, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE) {
        char infoLog[512];

        glGetShaderInfoLog(fragmentId, 512, nullptr, infoLog);
        std::cout << "Vertex error\n" << infoLog << std::endl;

        glDeleteShader(vertexId);

        return;
    }

    this->fragmentId = glCreateShader(GL_FRAGMENT_SHADER);

    std::string fragmentString = getShaderSource("resources/shaders/" + shaderName + "/fragment.glsl");
    const char *fragmentSource = fragmentString.c_str();
    glShaderSource(fragmentId, 1, &fragmentSource, 0);
    glCompileShader(fragmentId);
    glGetShaderiv(fragmentId, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE) {
        char infoLog[512];

        glGetShaderInfoLog(fragmentId, 512, nullptr, infoLog);
        std::cout << "Fragment error\n" << infoLog << std::endl;

        glDeleteShader(fragmentId);

        return;
    }

    glAttachShader(programId, vertexId);
    glAttachShader(programId, fragmentId);

    glBindAttribLocation(programId, 0, "vertices");
    glBindAttribLocation(programId, 1, "textures");

    glLinkProgram(programId);
    glGetProgramiv(programId, GL_LINK_STATUS, &isCompiled);
    if (!isCompiled) {
        char infoLog[512];

        glGetProgramInfoLog(programId, 512, nullptr, infoLog);
        std::cout << "Link error\n" << infoLog << std::endl;
    }
}

Shader::~Shader() {
    glDeleteShader(vertexId);
    glDeleteShader(fragmentId);
    glDeleteProgram(programId);
}

std::string Shader::getShaderSource(std::string filename) {
    std::ifstream file{filename};

    if (!file.is_open()) {
        std::cout << "File not found " << filename << std::endl;
        return nullptr;
    }

    std::string line{};
    std::string fileContent{};
    while (std::getline(file, line)) {
        fileContent += line;
        fileContent.push_back('\n');
    }
    file.close();

    return fileContent;
}

void Shader::use() {
    glUseProgram(programId);
}

void Shader::setUniform(std::string uniformName, int value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1)
        glUniform1i(location, value);
}

void Shader::setUniform(std::string uniformName, float value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1)
        glUniform1f(location, value);
}

void Shader::setUniform(std::string uniformName, glm::vec3 value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1) {
        glUniform3f(location, value.x, value.y, value.z);
    }
}

void Shader::setUniform(std::string uniformName, glm::vec4 value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1) {
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }
}

void Shader::setUniform(std::string uniformName, glm::vec2 value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1) {
        glUniform2f(location, value.x, value.y);
    }
}

void Shader::setUniform(std::string uniformName, glm::mat4 value) {
    int location = glGetUniformLocation(programId, uniformName.c_str());
    if (location != -1) {
        glUniformMatrix4fv(location,1,GL_FALSE, glm::value_ptr(value));
    }
}

int Shader::getAttribLocation(std::string attribName) {
    return glGetAttribLocation(programId, attribName.c_str());
}