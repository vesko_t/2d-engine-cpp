//
// Created by vesko on 8.11.2018 г..
//

#ifndef OPENGLTESTS_RENDER_H
#define OPENGLTESTS_RENDER_H

#include <fstream>
#include <iostream>
#include <string>
#include <GLM/vec3.hpp>
#include <GLM/vec2.hpp>
#include <GLM/vec4.hpp>
#include <GLM/mat4x4.hpp>

#include "../graphics/animations/Animations.h"

class Texture {
private:
    unsigned int textureId;
    int width;
    int height;
public:
    Texture(unsigned int);

    Texture(std::string);

    ~Texture();

    void bind(int);

    void bind();

    int getTextureId() const;

    void setTextureId(unsigned int textureId);

    int getWidth() const;

    void setWidth(int width);

    int getHeight() const;

    void setHeight(int height);

};

class Shader {
private:
    unsigned int programId;
    unsigned int vertexId;
    unsigned int fragmentId;

    std::string getShaderSource(std::string filename);

public:
    explicit Shader(std::string shaderName);

    ~Shader();

    void setUniform(std::string uniformName, int value);

    void setUniform(std::string uniformName, float value);

    void setUniform(std::string uniformName, glm::vec4);

    void setUniform(std::string uniformName, glm::vec3);

    void setUniform(std::string uniformName, glm::vec2);

    void setUniform(std::string uniformName, glm::mat4);

    void use();

    int getAttribLocation(std::string);
};

class Renderable {
private:
    glm::mat4 model;
    glm::mat4 view;
    glm::vec2 velocity;
    glm::vec2 position;
    Texture texture;
    glm::vec2 scale;
    Animation animation;
public:
    Renderable();

    const glm::mat4 &getModel() const;

    const glm::mat4 &getView() const;

    const glm::vec2 &getVelocity() const;

    void setVelocity(const glm::vec2 &velocity);

    const glm::vec2 &getPosition() const;

    void setPosition(const glm::vec2 &position);
};

class Model {
private:
    unsigned int textureVboId;
    unsigned int vertexVboId;
    unsigned int count;
public:
    explicit Model();
    ~Model();
    void render();

    void setTextureVboId(unsigned int textureVboId);
};

class Camera{
private:
    static glm::vec2 position;
    static glm::mat4 projection;
    static const int width{1280};
    static const int height{720};
public:
    Camera();

    static const glm::vec2 &getPosition();

    static void setPosition(const glm::vec2 &position);

    static const glm::mat4 &getProjection();

    static int getWidth();

    static int getHeight();
};
#endif //OPENGLTESTS_RENDER_H
