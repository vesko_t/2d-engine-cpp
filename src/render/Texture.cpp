//
// Created by vesko on 7.11.2018 г..
//
#define STB_IMAGE_IMPLEMENTATION
#include "Render.h"
#include <string>
#include <fstream>
#include <iostream>
#include <GL/glew.h>
#include <STB/stb_image.h>
#include <GL/gl.h>

Texture::Texture(unsigned int tex) : textureId{tex}, width{0}, height{0} {}

Texture::Texture(std::string fileName) : textureId{0}, width{0}, height{0} {

    unsigned char *texture {nullptr};

    texture = stbi_load(fileName.c_str(),&width,&height, nullptr, STBI_rgb_alpha);

    glGenTextures(1, &textureId);

    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(texture);
}

void Texture::bind() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
}

void Texture::bind(int sampler) {
    glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + sampler));
    glBindTexture(GL_TEXTURE_2D, textureId);
}

Texture::~Texture() {
    glDeleteTextures(1, &textureId);
}

int Texture::getTextureId() const {
    return textureId;
}

void Texture::setTextureId(unsigned int textureId) {
    glDeleteTextures(1, &this->textureId);
    this->textureId = textureId;
}

int Texture::getWidth() const {
    return width;
}

void Texture::setWidth(int width) {
    Texture::width = width;
}

int Texture::getHeight() const {
    return height;
}

void Texture::setHeight(int height) {
    Texture::height = height;
}
