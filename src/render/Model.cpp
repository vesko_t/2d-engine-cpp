//
// Created by vesko on 8.11.2018 г..
//

#include <GL/glew.h>
#include "Render.h"

Model::Model() : vertexVboId{0}, textureVboId{0}, count{0} {
    float vertices[] = {
            -1, 1,
            1, 1,
            1, -1,
            -1, -1
    };

    float textures[] = {
            0, 0,
            1, 0,
            1, 1,
            0, 1
    };

    count = sizeof(vertices) / sizeof(float);

    glGenBuffers(1, &vertexVboId);
    glGenBuffers(1, &textureVboId);

    glBindBuffer(GL_ARRAY_BUFFER, vertexVboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, textureVboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(textures), textures, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Model::render() {
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, vertexVboId);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindBuffer(GL_ARRAY_BUFFER, textureVboId);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glDrawArrays(GL_QUADS, 0, count);

    glBindVertexArray(0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

Model::~Model() {
    glDeleteBuffers(1, reinterpret_cast<const GLuint *>(vertexVboId));
    glDeleteBuffers(1, reinterpret_cast<const GLuint *>(textureVboId));
}

void Model::setTextureVboId(unsigned int textureVboId) {
    glDeleteBuffers(1, reinterpret_cast<const GLuint *>(this->textureVboId));
    Model::textureVboId = textureVboId;
}

