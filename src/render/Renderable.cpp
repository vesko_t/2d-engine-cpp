//
// Created by vesko on 8.11.2018 г..
//

#include "Render.h"

Renderable::Renderable(): {}

const glm::mat4 &Renderable::getModel() const {
    return model;
}

const glm::mat4 &Renderable::getView() const {
    return view;
}

const glm::vec2 &Renderable::getVelocity() const {
    return velocity;
}

void Renderable::setVelocity(const glm::vec2 &velocity) {
    Renderable::velocity = velocity;
}

const glm::vec2 &Renderable::getPosition() const {
    return position;
}

void Renderable::setPosition(const glm::vec2 &position) {
    Renderable::position = position;
}
