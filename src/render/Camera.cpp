//
// Created by vesko on 8.11.2018 г..
//

#include <GLM/gtc/matrix_transform.hpp>
#include "Render.h"

glm::mat4 Camera::projection{1.0f};
glm::vec2 Camera::position{0.0f};

Camera::Camera() {
    projection = glm::ortho(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f);
    position = glm::vec2(0.0f);
}

const glm::vec2 &Camera::getPosition() {
    return position;
}

void Camera::setPosition(const glm::vec2 &pos) {
    position = pos;
    projection = glm::translate(projection, glm::vec3(position,0.0f));
}

const glm::mat4 &Camera::getProjection() {
    return projection;
}

int Camera::getWidth() {
    return width;
}

int Camera::getHeight() {
    return height;
}
