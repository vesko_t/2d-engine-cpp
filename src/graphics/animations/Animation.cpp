//
// Created by vesko on 8.11.2018 г..
//
#include "Animations.h"
#include <time.h>

Animation::Animation(double FPS, int totalFrames) : FPS{FPS},
                                                    timesToRun{-1},
                                                    timesRan{0},
                                                    lastTime{0},
                                                    running{false},
                                                    elapsedTime{0},
                                                    currentTime{0},
                                                    totalFrames{totalFrames} {}

Animation::Animation(double FPS, int timesToRun, int totalFrames) : FPS{FPS},
                                                                    timesToRun{timesToRun - 1},
                                                                    timesRan{0},
                                                                    lastTime{0},
                                                                    running{false},
                                                                    elapsedTime{0},
                                                                    currentTime{0},
                                                                    totalFrames{totalFrames}{}

void Animation::start() {
    lastTime = time(nullptr);
    pointer = 0;
    running = true;
    timesRan = 0;
}

void Animation::stop() {
    pointer = 0;
    running = false;
    timesRan = 0;
}

void Animation::render() {
    if (running) {
        currentTime = time(nullptr);
        this->elapsedTime += currentTime - lastTime;

        if (elapsedTime >= FPS) {
            elapsedTime -= FPS;
            pointer++;
        }

        if (pointer >= totalFrames) {
            pointer = 0;
            if (timesToRun != -1) {
                timesRan++;
                if (timesRan > timesToRun) {
                    running = false;
                    return;
                }
            }
        }
        lastTime = currentTime;

        action(pointer);
    }
}

