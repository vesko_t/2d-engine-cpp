//
// Created by vesko on 8.11.2018 г..
//

#include "Animations.h"
#include "../../util/Util.h"

TextureAnimation::TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps,
                                   Renderable renderable) : Animation(fps, totalCols), renderable{renderable},
                                                            spriteSheet{spriteSheet}, frames{} {
    createFrames(cols, rows, totalCols, startingRow);
}

TextureAnimation::TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps,
                                   Renderable renderable, int timesToRun) : Animation(fps, timesToRun, totalCols),
                                                                            spriteSheet{spriteSheet},
                                                                            renderable{renderable}, frames{} {
    createFrames(cols, rows, totalCols, startingRow);
}

void TextureAnimation::createFrames(int cols, int rows, int totalCols, int startingRow) {
    float glWidth = ((float) spriteSheet.getWidth() / (float) totalCols) / (float) spriteSheet.getWidth();
    float glHeight = ((float) spriteSheet.getHeight() / (float) rows) / (float) spriteSheet.getHeight();

    for (int i = 0; i < cols; i++) {
        float textureCoordinates[] = {
                i * glWidth, startingRow * glHeight,
                i * glWidth + glWidth, startingRow * glHeight,
                i * glWidth + glWidth, startingRow * glHeight + glHeight,
                i * glWidth, startingRow * glHeight + glHeight
        };
        frames.push_back(createVbo(textureCoordinates));
    }

}

void TextureAnimation::action(int pointer) {
    spriteSheet.bind(4);
    renderable.getModel().
}