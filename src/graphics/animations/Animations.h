//
// Created by vesko on 8.11.2018 г..
//

#ifndef OPENGLTESTS_ANIMATIONS_H
#define OPENGLTESTS_ANIMATIONS_H

#include <vector>
#include <string>
#include "../../render/Render.h"

class Animation {
private:
    int pointer;

    int timesToRun;

    int timesRan;

    bool running;

    int totalFrames;

    double elapsedTime, currentTime, lastTime, FPS;
protected:
    virtual void action(int pointer) = 0;
public:
    Animation(double FPS, int totalFrames);
    Animation(double FPS, int timesToRun, int totalFrames);
    void start();
    void stop();
    void render();
};

class TextureAnimation: Animation{
private:
    std::vector<unsigned int> frames;
    Texture spriteSheet;
    Renderable renderable;
    void createFrames();
protected:
private:
    void action(int pointer) override;

public:
    TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps, Renderable renderable);
    TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps, Renderable renderable, int timesToRun);
    void createFrames(int cols, int rows, int totalCols, int startingRow);
};

#endif //OPENGLTESTS_ANIMATIONS_H
