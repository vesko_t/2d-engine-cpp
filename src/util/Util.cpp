//
// Created by vesko on 8.11.2018 г..
//

#include <GL/glew.h>
#include "Util.h"

unsigned int createVbo(float data[]){
    unsigned int vboiId{0};
    glGenBuffers(1,&vboiId);

    glBindBuffer(GL_ARRAY_BUFFER, vboiId);
    glBufferData(GL_ARRAY_BUFFER,sizeof(data), data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vboiId;
}