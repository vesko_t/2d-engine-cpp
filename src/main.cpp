#include <iostream>
#include <string>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <GLM/mat4x4.hpp>
#include <GLM/geometric.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/vec3.hpp>
#include "render/Render.h"

const int width{1280};
const int height{720};

int main() {
    GLFWwindow *window;

    if (!glfwInit())
        return -1;

    window = glfwCreateWindow(width, height, "Random game", nullptr, nullptr);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    const GLFWvidmode *videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    glfwSetWindowPos(window, (videoMode->width - width) / 2, (videoMode->height - height) / 2);

    glfwMakeContextCurrent(window);

    glewInit();

    Shader shader{"test"};

    Texture texture{"resources/images/tile2.png"};

    glm::mat4 projection = glm::ortho(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f);
    glm::mat4 model{1.0f};
    glm::mat4 view = glm::scale(glm::mat4(1.0f), glm::vec3(32));

    glm::vec3 position{0};

    Model rawModel{};

    while (!glfwWindowShouldClose(window)) {

        if (glfwGetKey(window, GLFW_KEY_D)) {
            position += glm::vec3(0.000001f, 0, 0);
        } else if (glfwGetKey(window, GLFW_KEY_A)) {
            position -= glm::vec3(0.000001f, 0, 0);
        }

        glClear(GL_COLOR_BUFFER_BIT);

        model = glm::translate(glm::mat4(1.0f), position);

        shader.use();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", projection);
        shader.setUniform("model", model);
        shader.setUniform("view", view);
        texture.bind();
        rawModel.render();

        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}


